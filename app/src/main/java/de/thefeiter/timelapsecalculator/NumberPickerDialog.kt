package de.thefeiter.timelapsecalculator

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.widget.NumberPicker
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class NumberPickerDialog(
    private val ctx: Context,
    private val minValue:Int,
    private val maxValue:Int,
    private var defaultValue: Int,
    private val onPick: (value:Int) -> Unit
) : DialogFragment(){

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {


        val builder = AlertDialog.Builder(ctx)

        val inflater = this.layoutInflater
        val dialogLayout = inflater.inflate(R.layout.dialog_number_picker, null)


        val numberPicker : NumberPicker = dialogLayout.findViewById(R.id.numberPicker)

        numberPicker.minValue = minValue
        numberPicker.maxValue = maxValue
        numberPicker.value = defaultValue

        builder.setView(dialogLayout)
        builder.setPositiveButton("Done") { dialog: DialogInterface?, id: Int ->
            onPick.invoke(
                numberPicker.value
            )
        }

        return builder.create()
    }
}