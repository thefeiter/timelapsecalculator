package de.thefeiter.timelapsecalculator

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.widget.NumberPicker
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class TimeDeltaPickerDialog(private val ctx: Context, private var defaultSeconds: Int,  private val onTimePick: (seconds:Int) -> Unit) : DialogFragment(){

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {


        val builder = AlertDialog.Builder(ctx)

        val inflater = this.layoutInflater
        val dialogLayout = inflater.inflate(R.layout.dialog_timedelta_picker, null)


        val hourPicker : NumberPicker = dialogLayout.findViewById(R.id.timePickerHours)
        val minutePicker : NumberPicker = dialogLayout.findViewById(R.id.timePickerMinutes)
        val secondPicker : NumberPicker = dialogLayout.findViewById(R.id.timePickerSeconds)

        hourPicker.maxValue = 1000
        hourPicker.minValue = 0

        minutePicker.maxValue = 59
        minutePicker.minValue = 0

        secondPicker.maxValue = 59
        secondPicker.minValue = 0



        hourPicker.value = defaultSeconds/3600
        defaultSeconds -= hourPicker.value * 3600
        minutePicker.value = defaultSeconds/60
        secondPicker.value = defaultSeconds- minutePicker.value*60

        builder.setView(dialogLayout)
        builder.setPositiveButton("Done") { dialog: DialogInterface?, id: Int ->
            onTimePick.invoke(
                hourPicker.value*3600+
                        minutePicker.value*60+
                        secondPicker.value
            )
        }

        return builder.create()
    }
}