package de.thefeiter.timelapsecalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlin.math.min

class MainActivity : AppCompatActivity() {


    var recordDuration = 0
    var outputDuration = 60
    var fps = 25
    lateinit var recordDurationView: TextView
    lateinit var outputDurationView: TextView
    lateinit var fpsView: TextView
    lateinit var calcImagesView: TextView
    lateinit var calcSpeedView: TextView
    lateinit var calcDelayView: TextView
    lateinit var calcButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recordDurationView = findViewById(R.id.recDuration)
        outputDurationView = findViewById(R.id.outDuration)
        fpsView =  findViewById(R.id.fps)
        calcImagesView  = findViewById(R.id.images)
        calcSpeedView  = findViewById(R.id.speed)
        calcDelayView  = findViewById(R.id.delay)
        calcButton = findViewById(R.id.calculate)

        recordDurationView.setOnClickListener{
            val timePickDialog = TimeDeltaPickerDialog(this, recordDuration){
                recordDuration = it
                updateView()
            }
            timePickDialog.show(supportFragmentManager, null)
        }

        outputDurationView.setOnClickListener{
            val timePickDialog = TimeDeltaPickerDialog(this, outputDuration){
                outputDuration = it
                updateView()
            }
            timePickDialog.show(supportFragmentManager, null)
        }

        fpsView.setOnClickListener{
            val numberPickerDialog = NumberPickerDialog(this,1,50,  fps){
                fps = it
                updateView()
            }
            numberPickerDialog.show(supportFragmentManager, null)
        }




        calcButton.setOnClickListener {



            val images: Int = outputDuration * fps

            val delaytime: Float = recordDuration.toFloat() / images.toFloat()

            val speed: Float = recordDuration.toFloat() / outputDuration.toFloat()

            calcImagesView.text = images.toString()
            calcSpeedView.text = speed.toString()
            calcDelayView.text = delaytime.toString()


        }

        updateView()


    }

    fun updateView(){

        recordDurationView.text = toTimeString(recordDuration)
        outputDurationView.text = toTimeString(outputDuration)
        fpsView.text = fps.toString()

    }

    private fun toTimeString(seconds: Int) : String{
        var remainSeconds: Int = seconds

        val hours: Int = remainSeconds/3600
        remainSeconds -= hours * 3600

        val minutes: Int = remainSeconds/60
        remainSeconds -= minutes * 60


        return "$hours : $minutes : $remainSeconds"

    }


}